package com.demo.spring.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="customer")
public class customers{

	@Id
	private Integer customer_Id;
	private String password;
	private String name;
	private String mail;
	private String address;
	private Double phone_No;
	private String dob;
	private String gender;
	
	public customers(Integer customer_Id, String password, String name, String mail, String address, Double phone_No,
			String dob, String gender) {
		this.customer_Id = customer_Id;
		this.password = password;
		this.name = name;
		this.mail = mail;
		this.address = address;
		this.phone_No = phone_No;
		this.dob = dob;
		this.gender = gender;
	}
	public customers() {
		// TODO Auto-generated constructor stub
	}
	public Integer getCustomer_Id() {
		return customer_Id;
	}
	public void setCustomer_Id(Integer customer_Id) {
		this.customer_Id = customer_Id;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Double getPhone_No() {
		return phone_No;
	}
	public void setPhone_No(Double phone_No) {
		this.phone_No = phone_No;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	
	
	
}
