package com.demo.spring.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="beneficiarydetail")
public class Beneficiary {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "my_id_table")
	private Integer beneficiary_id;
	public Integer getBeneficiary_id() {
		return beneficiary_id;
	}

	public void setBeneficiary_id(Integer beneficiary_id) {
		this.beneficiary_id = beneficiary_id;
	}

	private Integer user_Id;
	private Integer account_Id;
	private String name;
	private String ifsc_code;
	
	public Beneficiary() {
		// TODO Auto-generated constructor stub
	}

	public Beneficiary(Integer user_Id, Integer account_Id, String name, String ifsc_code) {
		super();
		this.user_Id = user_Id;
		this.account_Id = account_Id;
		this.name = name;
		this.ifsc_code = ifsc_code;
	}

	public Integer getUser_Id() {
		return user_Id;
	}

	public void setUser_Id(Integer user_Id) {
		this.user_Id = user_Id;
	}

	public Integer getAccount_Id() {
		return account_Id;
	}

	public void setAccount_Id(Integer account_Id) {
		this.account_Id = account_Id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIfsc_code() {
		return ifsc_code;
	}

	public void setIfsc_code(String ifsc_code) {
		this.ifsc_code = ifsc_code;
	}
	
	
}

