package com.demo.spring.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="TRANSACTION")
public class Transaction {


	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "my_id_table_Transaction")
	private Integer S_No;
	private Integer transaction_Id;
	private Integer from_Account;
	private Integer to_Account;
	private Integer amount;
	private String transaction_Time;
	private Integer account_Balance;
	private Integer status;
	private String remarks;
	private Integer flag;
	
	
	public Transaction() {
		// TODO Auto-generated constructor stub
	}

	public Transaction(Integer transaction_Id, Integer from_Account, Integer to_Account, String transaction_Type,
			Integer amount, String transaction_Time, Integer account_Balance, Integer status, String remarks) {
		super();
		this.transaction_Id = transaction_Id;
		this.from_Account = from_Account;
		this.to_Account = to_Account;
		this.amount = amount;
		this.transaction_Time = transaction_Time;
		this.account_Balance = account_Balance;
		this.status = status;
		this.remarks = remarks;
	}
	public Transaction(Transaction tran) {
		// TODO Auto-generated constructor stub

		this.transaction_Id = tran.transaction_Id;
		this.from_Account = tran.from_Account;
		this.to_Account = tran.to_Account;
		this.amount = tran.amount;
		this.transaction_Time = tran.transaction_Time;
		this.account_Balance = tran.account_Balance;
		this.status = tran.status;
		this.remarks = tran.remarks;
	}

	
	public Integer getFlag() {
		return flag;
	}

	public void setFlag(Integer flag) {
		this.flag = flag;
	}

	public Integer getS_No() {
		return S_No;
	}

	public void setS_No(Integer s_No) {
		S_No = s_No;
	}

	public Integer getTransaction_Id() {
		return transaction_Id;
	}

	public void setTransaction_Id(Integer transaction_Id) {
		this.transaction_Id = transaction_Id;
	}

	public Integer getFrom_Account() {
		return from_Account;
	}

	public void setFrom_Account(Integer from_Account) {
		this.from_Account = from_Account;
	}

	public Integer getTo_Account() {
		return to_Account;
	}

	public void setTo_Account(Integer to_Account) {
		this.to_Account = to_Account;
	}

	
	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public String getTransaction_Time() {
		return transaction_Time;
	}

	public void setTransaction_Time(String transaction_Time) {
		this.transaction_Time = transaction_Time;
	}

	public Integer getAccount_Balance() {
		return account_Balance;
	}

	public void setAccount_Balance(Integer account_Balance) {
		this.account_Balance = account_Balance;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
	
	
}
