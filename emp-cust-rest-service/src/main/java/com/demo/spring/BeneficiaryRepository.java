package com.demo.spring;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.demo.spring.entity.Beneficiary;

public interface BeneficiaryRepository extends JpaRepository<Beneficiary, Integer> {

	
	@Query("select e from Beneficiary e where  e.user_Id=?1")
 	public List<Beneficiary> findBeneficiaries(Integer user_Id);
}
