package com.demo.spring;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.demo.spring.entity.Transaction;

public interface TransactionRepository extends JpaRepository<Transaction, Integer> {

	@Query("select t from Transaction t where t.transaction_Time  between  ?2 and ?3 and from_account=?1 ")
	public List<Transaction> findAllTransaction(Integer accId,String start,String end);
	
}

