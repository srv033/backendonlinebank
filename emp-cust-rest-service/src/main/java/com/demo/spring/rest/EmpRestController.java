package com.demo.spring.rest;

import java.util.List;
import java.util.Optional;

import com.demo.spring.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.demo.spring.AccountRepository;
import com.demo.spring.BeneficiaryRepository;
import com.demo.spring.CustomersRepository;
import com.demo.spring.TransactionRepository;
import com.demo.spring.entity.AccountList;
import com.demo.spring.entity.Beneficiary;
import com.demo.spring.entity.Transaction;
import com.demo.spring.entity.accounts;
import com.demo.spring.entity.customerList;
import com.demo.spring.entity.customers;

@RestController
public class EmpRestController {

	@Autowired
	private CustomersRepository repo;

	@Autowired
	private AccountRepository accRepo;

	@Autowired
	private TransactionRepository transRepo;

	@Autowired
	private BeneficiaryRepository benRepo;

//	@GetMapping(path="/emp/find/{id}",produces = {MediaType.APPLICATION_JSON_VALUE,MediaType.APPLICATION_XML_VALUE})
//	public ResponseEntity findEmp(@PathVariable("id") int id)  {
//		Optional<Emp> o=repo.findById(id);
//		if(o.isPresent()) {
//			return ResponseEntity.ok(o.get());
//			
//		}
//		else {
//			throw new RuntimeException("Emp not found");
//		}
//	}
//	
//	@GetMapping(path="/emp",produces= {MediaType.APPLICATION_JSON_VALUE,MediaType.APPLICATION_XML_VALUE})
//	public ResponseEntity<MyList> getList(){
//		List<Emp> empList=repo.findAll();
//		MyList mylist =new MyList();
//		mylist.setEmplist(empList);
//		return ResponseEntity.ok(mylist);
//	}

	@GetMapping(path = "/customers", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<customerList> getcustomers() {
		List<customers> cusList = repo.findAll();
		customerList customerlist = new customerList();
		customerlist.setCus(cusList);
		// System.out.println("asad"+accList);
		return ResponseEntity.ok(customerlist);
	}

	@GetMapping(path = "/accounts", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AccountList> getaccounts() {
		List<accounts> accList = accRepo.findAll();
		AccountList accountlist = new AccountList();
		accountlist.setAcc(accList);
		// System.out.println("asad"+accList);
		return ResponseEntity.ok(accountlist);
	}

	@GetMapping(path = "/customer/accounts/{cId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AccountList> customer_Accounts(@PathVariable("cId") int cid) {

		accounts a = new accounts();
		a.setCustomer_Id(cid);
		Example<accounts> example = Example.of(a);
		List<accounts> accList = accRepo.findAll(example);
		AccountList accountlist = new AccountList();
		accountlist.setAcc(accList);
		// System.out.println("asad"+accList);
		return ResponseEntity.ok(accountlist);
	}

	@GetMapping(path = "/customer/account/transaction", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Transaction>> Transaction_details() {



		Integer accId = 1001;
		String start = "2012-11-19";
		String end = "2022-06-19";
		List<Transaction> accList = transRepo.findAllTransaction(accId, start, end);

		return ResponseEntity.ok(accList);
	}

	@PostMapping(path = "/customer/transfer", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Message> doTransaction(@RequestBody Transaction transaction) {

		System.out.println("Transaction:" + transaction.getAmount());
		// transaction.setTransaction_Time();
		Optional<accounts> op_send = accRepo.findById(transaction.getFrom_Account());
		accounts acc_sender = op_send.get();

		Optional<accounts> op_recv = accRepo.findById(transaction.getTo_Account());
		accounts acc_receiver = op_recv.get();

		Message message = new Message();

		if (acc_sender.getAccount_Balance() > transaction.getAmount() && acc_sender.getAccount_Balance()>=5000) {
			acc_sender.setAccount_Balance(acc_sender.getAccount_Balance() - transaction.getAmount());

			acc_receiver.setAccount_Balance(acc_receiver.getAccount_Balance() + transaction.getAmount());

			accRepo.save(acc_sender);

			accRepo.save(acc_receiver);

			Transaction t_sender = new Transaction(transaction);

			Transaction t_receiver = new Transaction(transaction);

			if (transaction.getAmount() > 10000) {

				t_sender.setFlag(1);
				t_receiver.setFlag(1);

			}

			t_sender.setAccount_Balance(acc_sender.getAccount_Balance());

			t_receiver.setAccount_Balance(acc_receiver.getAccount_Balance());

			transRepo.save(t_sender);

			transRepo.save(t_receiver);
			message.setStatus("Transaction Done");

			return ResponseEntity.ok(message);
		} else {
			message.setStatus("Transaction Cannot be done");

			return ResponseEntity.ok(message);

		}
	}

	@PostMapping(path = "/customer/addbeneficiar", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Message> saveEmp(@RequestBody Beneficiary ben) {

		Beneficiary benf = new Beneficiary();
		benf.setUser_Id(ben.getUser_Id());
		benf.setAccount_Id(ben.getAccount_Id());
		Example<Beneficiary> example = Example.of(benf);
		Message message = new Message();

		if (benRepo.exists(example)) {
			message.setStatus("Employee Already Exists");
			return ResponseEntity.ok(message);

		} else {
			benRepo.save(ben);

			message.setStatus("Employee saved");
			return ResponseEntity.ok(message);
		}
	}

	@GetMapping(path = "/customer/getbeneficiar/{user_id}", produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<List<Beneficiary>> getBeneficiar(@PathVariable("user_id") Integer user_Id) {

		List<Beneficiary> benList = benRepo.findBeneficiaries(user_Id);
		return ResponseEntity.ok(benList);
	}

//	@GetMapping(path="/emp/list",produces= {MediaType.APPLICATION_JSON_VALUE,MediaType.APPLICATION_XML_VALUE})
//	public ResponseEntity<MyList> getListCity(@RequestParam("city") String city){
//		
//		//
//		
//		Emp e =new Emp();
//		e.setCity(city);
////		//emps.setName("banglore");
//		Example<Emp> example = Example.of(e);
//		
//		List<Emp> empList=repo.findAll(example);
//		MyList mylist =new MyList();
//		mylist.setEmplist(empList);
//		return ResponseEntity.ok(mylist);
//	}
//	
////	
////	@GetMapping(path="/emp/",produces = MediaType.APPLICATION_JSON_VALUE)
////	public ResponseEntity getEmp()  {
////		List<Emp> emps=repo.findAll();
////		//Optional<Emp> e=repo.findAll();
////		return ResponseEntity.ok(emps);
////	}
//	
//	
//	@PostMapping(path="/emp/save",produces = MediaType.APPLICATION_JSON_VALUE,consumes=MediaType.APPLICATION_JSON_VALUE)
//	public ResponseEntity<Message> saveEmp(@RequestBody Emp e){
//		if(repo.existsById(e.getEmpId())){
//			
//			Message message = new Message();
//			message.setStatus("Employee exists");
//			return ResponseEntity.ok(message);		}
//		else {
//			repo.save(e);
//			Message message = new Message();
//			message.setStatus("Employee saved");
//			return ResponseEntity.ok(message);	
//		}	
//	}
//	
//	
//	@PutMapping(path="/emp/update",produces = MediaType.APPLICATION_JSON_VALUE,consumes=MediaType.APPLICATION_JSON_VALUE)
//	public ResponseEntity<Message> updateEmp(@RequestBody Emp e){
//			repo.save(e);
//			Message message = new Message();
//			message.setStatus("Employee updated");
//			return ResponseEntity.ok(message);		
//			
//	}
//	
//	
//	@DeleteMapping(path="/emp/delete/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
//	public ResponseEntity<Message> delete(@PathVariable("id") int id){
//			if(repo.existsById(id)) {
//				repo.deleteById(id);
//				Message message = new Message();
//				message.setStatus("Employee deleted");
//				return ResponseEntity.ok(message);	
//			}	
//			else {
//				Message message = new Message();
//				message.setStatus("Employee not found");
//				return ResponseEntity.ok(message);
//			}
//			
//	}

//	@ExceptionHandler(RuntimeException.class)
//	public ResponseEntity handleEmpNotFound(RuntimeException ex) {
//		return ResponseEntity.ok("no accounts registered");
//	}
}
