package com.demo.spring.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="accounts")
public class accounts {

	@Id
	private Integer account_Id;
	private String branch;
	private Integer customer_Id;
	private Integer account_Balance;
	private String account_Type;
	private String opened_date;
	private Integer status;
		
	public accounts() {
		// TODO Auto-generated constructor stub
	}
	public accounts(Integer account_Id, String branch, Integer customer_Id, Integer account_Balance, String account_Type,
			String opened_date, Integer status) {
		this.account_Id = account_Id;
		this.branch = branch;
		this.customer_Id = customer_Id;
		this.account_Balance = account_Balance;
		this.account_Type = account_Type;
		this.opened_date = opened_date;
		this.status = status;
	}
	public Integer getAccount_Id() {
		return account_Id;
	}
	public void setAccount_Id(Integer account_Id) {
		this.account_Id = account_Id;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public Integer getCustomer_Id() {
		return customer_Id;
	}
	public void setCustomer_Id(Integer customer_Id) {
		this.customer_Id = customer_Id;
	}
	public Integer getAccount_Balance() {
		return account_Balance;
	}
	public void setAccount_Balance(Integer account_Balance) {
		this.account_Balance = account_Balance;
	}
	public String getAccount_Type() {
		return account_Type;
	}
	public void setAccount_Type(String account_Type) {
		this.account_Type = account_Type;
	}
	public String getOpened_date() {
		return opened_date;
	}
	public void setOpened_date(String opened_date) {
		this.opened_date = opened_date;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	
	
	
}

