package com.demo.spring.rest;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.demo.spring.AccountRepository;
import com.demo.spring.entity.AccountList;
import com.demo.spring.entity.accounts;

@RestController
public class EmpRestController {

	@Autowired
	private AccountRepository repo;
//	@GetMapping(path="/emp/find/{id}",produces = {MediaType.APPLICATION_JSON_VALUE,MediaType.APPLICATION_XML_VALUE})
//	public ResponseEntity findEmp(@PathVariable("id") int id)  {
//		Optional<Emp> o=repo.findById(id);
//		if(o.isPresent()) {
//			return ResponseEntity.ok(o.get());
//			
//		}
//		else {
//			throw new RuntimeException("Emp not found");
//		}
//	}
//	
//	@GetMapping(path="/emp",produces= {MediaType.APPLICATION_JSON_VALUE,MediaType.APPLICATION_XML_VALUE})
//	public ResponseEntity<MyList> getList(){
//		List<Emp> empList=repo.findAll();
//		MyList mylist =new MyList();
//		mylist.setEmplist(empList);
//		return ResponseEntity.ok(mylist);
//	}
	
	@GetMapping(path="/accounts",produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AccountList> getaccounts(){
		List<accounts> accList=repo.findAll();
		AccountList accountlist =new AccountList();
		accountlist.setAcc(accList);
		System.out.println("asad"+accList);
		return ResponseEntity.ok(accountlist);
	}
	
//	@GetMapping(path="/emp/list",produces= {MediaType.APPLICATION_JSON_VALUE,MediaType.APPLICATION_XML_VALUE})
//	public ResponseEntity<MyList> getListCity(@RequestParam("city") String city){
//		
//		//
//		
//		Emp e =new Emp();
//		e.setCity(city);
////		//emps.setName("banglore");
//		Example<Emp> example = Example.of(e);
//		
//		List<Emp> empList=repo.findAll(example);
//		MyList mylist =new MyList();
//		mylist.setEmplist(empList);
//		return ResponseEntity.ok(mylist);
//	}
//	
////	
////	@GetMapping(path="/emp/",produces = MediaType.APPLICATION_JSON_VALUE)
////	public ResponseEntity getEmp()  {
////		List<Emp> emps=repo.findAll();
////		//Optional<Emp> e=repo.findAll();
////		return ResponseEntity.ok(emps);
////	}
//	
//	
//	@PostMapping(path="/emp/save",produces = MediaType.APPLICATION_JSON_VALUE,consumes=MediaType.APPLICATION_JSON_VALUE)
//	public ResponseEntity<Message> saveEmp(@RequestBody Emp e){
//		if(repo.existsById(e.getEmpId())){
//			
//			Message message = new Message();
//			message.setStatus("Employee exists");
//			return ResponseEntity.ok(message);		}
//		else {
//			repo.save(e);
//			Message message = new Message();
//			message.setStatus("Employee saved");
//			return ResponseEntity.ok(message);	
//		}	
//	}
//	
//	
//	@PutMapping(path="/emp/update",produces = MediaType.APPLICATION_JSON_VALUE,consumes=MediaType.APPLICATION_JSON_VALUE)
//	public ResponseEntity<Message> updateEmp(@RequestBody Emp e){
//			repo.save(e);
//			Message message = new Message();
//			message.setStatus("Employee updated");
//			return ResponseEntity.ok(message);		
//			
//	}
//	
//	
//	@DeleteMapping(path="/emp/delete/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
//	public ResponseEntity<Message> delete(@PathVariable("id") int id){
//			if(repo.existsById(id)) {
//				repo.deleteById(id);
//				Message message = new Message();
//				message.setStatus("Employee deleted");
//				return ResponseEntity.ok(message);	
//			}	
//			else {
//				Message message = new Message();
//				message.setStatus("Employee not found");
//				return ResponseEntity.ok(message);
//			}
//			
//	}
	
//	@ExceptionHandler(RuntimeException.class)
//	public ResponseEntity handleEmpNotFound(RuntimeException ex) {
//		return ResponseEntity.ok("no accounts registered");
//	}
	}
	

