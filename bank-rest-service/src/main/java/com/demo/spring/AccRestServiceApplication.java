package com.demo.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
public class AccRestServiceApplication implements WebMvcConfigurer{

	public static void main(String[] args) {
		SpringApplication.run(AccRestServiceApplication.class, args);
	}
}
